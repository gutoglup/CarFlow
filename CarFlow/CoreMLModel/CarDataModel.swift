//
//  CarDataModel.swift
//  CarFlow
//
//  Created by Augusto Reis on 01/07/2018.
//  Copyright © 2018 Augusto Reis. All rights reserved.
//

import Foundation
import Vision

struct CarDataModel {
    
    static func carListRepresentation(observationList: [VNClassificationObservation]) -> [Car]? {
        if observationList.isEmpty {
            return nil
        } else {
            let filterList = observationList.prefix(5)
            var carList = [Car]()
            if let carListPath = Bundle.main.path(forResource: "CarListDataModel", ofType: "json") {
                do {
                    let carListString = try String(contentsOfFile: carListPath, encoding: .utf8)
                    if  let carListData = carListString.data(using: .utf8),
                        let carListDataModelJSON = try JSONSerialization.jsonObject(with: carListData, options: .allowFragments) as? [String: Any] {
                        filterList.forEach { (observationItem) in
                            if let carItemDataModel = carListDataModelJSON[observationItem.identifier] as? [String: String],
                                let model = carItemDataModel["Model"],
                                let manufacture = carItemDataModel["Manufacture"]{
                                
                                carList.append(Car(model: model, fabric: manufacture))
                            }
                        }
                        return carList
                    }
                }catch {
                    return nil
                }
            }
        }
        return nil
    }
    
}
