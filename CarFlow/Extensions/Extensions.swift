//
//  Extensions.swift
//  CarFlow
//
//  Created by Augusto Reis on 01/07/2018.
//  Copyright © 2018 Augusto Reis. All rights reserved.
//

import UIKit

protocol ReusableView {}

extension ReusableView {
    static var identifier: String {
        return String(describing: self)
    }
}

extension UITableView {
    func dequeueReusableCell <T:UITableViewCell> (indexPath: IndexPath) -> T where T: ReusableView {
        guard let cell = self.dequeueReusableCell(withIdentifier: T.identifier, for: indexPath) as? T else {
            fatalError("The cell \(T.identifier) not found")
        }
        return cell
    }
    
    
    
}
