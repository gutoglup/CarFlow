//
//  Car.swift
//  CarFlow
//
//  Created by Augusto Reis on 01/07/2018.
//  Copyright © 2018 Augusto Reis. All rights reserved.
//

import Foundation
import RealmSwift

typealias RealmCompletionHandler = (Any?, Error?) -> Void

class Car: Object {
    @objc dynamic var model: String = ""
    @objc dynamic var fabric: String = ""
    @objc dynamic var year: Int = 0
    @objc dynamic var latitude: Double = 0
    @objc dynamic var longitude: Double = 0
    @objc dynamic var date: Date = Date()
    @objc dynamic var image: Data = Data()
    
    convenience init(model: String, fabric: String, year: Int, latitude: Double, longitude: Double, date: Date, image: Data) {
        self.init()
        self.model = model
        self.fabric = fabric
        self.year = year
        self.latitude = latitude
        self.longitude = longitude
        self.date = date
        self.image = image
    }

    convenience init(model: String, fabric: String) {
        self.init()
        self.model = model
        self.fabric = fabric
    }
    
    func saveCar(completionHandler: @escaping RealmCompletionHandler) {
        DispatchQueue.global(qos: .default).async {
            do {
                let realm = try Realm()
                try realm.write {
                    realm.add(self)
                }
                completionHandler(self, nil)
            } catch (let error) {
                completionHandler(nil, error)
            }
        }
    }
    
    static func listAllCars() -> Results<Car>? {
        do {
            let realm = try Realm()
            let carList = realm.objects(Car.self)
            return carList
        } catch (let error) {
            print(error)
            return nil
        }
    }

}
