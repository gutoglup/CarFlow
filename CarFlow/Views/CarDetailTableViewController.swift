//
//  CarDetailTableViewController.swift
//  CarFlow
//
//  Created by Augusto Reis on 03/07/2018.
//  Copyright © 2018 Augusto Reis. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField
import MapKit

class CarDetailTableViewController: UITableViewController, ReusableView {

    @IBOutlet weak var imageViewCar: UIImageView!
    @IBOutlet weak var textFieldModel: JVFloatLabeledTextField!
    @IBOutlet weak var textFieldFabric: JVFloatLabeledTextField!
    @IBOutlet weak var textFieldYear: JVFloatLabeledTextField!
    @IBOutlet weak var textFieldRegisterDate: JVFloatLabeledTextField!
    @IBOutlet weak var mapViewLocation: MKMapView!
    
    var car: Car?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let car = car {
            imageViewCar.image = UIImage(data: car.image)
            imageViewCar.layer.cornerRadius = imageViewCar.frame.size.height / 2
            textFieldModel.text = car.model
            textFieldFabric.text = car.fabric
            textFieldYear.text = car.year.string
            textFieldRegisterDate.text = car.date.string(withFormat: "dd/MM/yyyy")
            let coordinates = CLLocationCoordinate2D(latitude: car.latitude, longitude: car.longitude)
            let region = MKCoordinateRegion(center: coordinates, span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2))
            mapViewLocation.setRegion(region, animated: true)
            let pinAnnotation = MKPointAnnotation()
            pinAnnotation.coordinate = coordinates
            mapViewLocation.addAnnotation(pinAnnotation)
        }
    }


}
