//
//  CarListTableViewController.swift
//  CarFlow
//
//  Created by Augusto Reis on 01/07/2018.
//  Copyright © 2018 Augusto Reis. All rights reserved.
//

import UIKit
import RealmSwift

class CarListTableViewController: UITableViewController {

    var carList: Results<Car>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        carList = Car.listAllCars()
    }

    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return carList != nil ? 1 : 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let carList = carList {
            return carList.count
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath: indexPath) as CarTableViewCell
        if let car = carList?[indexPath.row] {
            cell.labelModel.text = car.model
            cell.labelFabric.text = car.fabric
            cell.labelYear.text = car.year.string
            cell.labelDate.text = car.date.string(withFormat: "dd/MM/yyyy")
            cell.imageViewCar.image = UIImage(data: car.image)
            cell.imageViewCar.layer.cornerRadius = cell.imageViewCar.frame.size.height/2
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let car = carList?[indexPath.row] {
            performSegue(withIdentifier: CarDetailTableViewController.identifier, sender: car)
        }
    }

    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0001
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let carDetailTableViewController = segue.destination as? CarDetailTableViewController,
            let car = sender as? Car {
            carDetailTableViewController.car = car
        }
    }
}
