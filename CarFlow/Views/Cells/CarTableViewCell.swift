//
//  CarTableViewCell.swift
//  CarFlow
//
//  Created by Augusto Reis on 03/07/2018.
//  Copyright © 2018 Augusto Reis. All rights reserved.
//

import UIKit

class CarTableViewCell: UITableViewCell, ReusableView {

    @IBOutlet weak var labelModel: UILabel!
    @IBOutlet weak var labelFabric: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelYear: UILabel!
    @IBOutlet weak var imageViewCar: UIImageView!
    
}
