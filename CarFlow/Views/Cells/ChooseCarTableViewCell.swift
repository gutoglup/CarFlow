//
//  ChooseCarTableViewCell.swift
//  CarFlow
//
//  Created by Augusto Reis on 01/07/2018.
//  Copyright © 2018 Augusto Reis. All rights reserved.
//

import UIKit

class ChooseCarTableViewCell: UITableViewCell, ReusableView {

    @IBOutlet weak var labelModel: UILabel!
    @IBOutlet weak var labelFabric: UILabel!
    
}
