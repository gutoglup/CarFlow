//
//  ChooseCarLocationViewController.swift
//  CarFlow
//
//  Created by Augusto Reis on 02/07/2018.
//  Copyright © 2018 Augusto Reis. All rights reserved.
//

import UIKit
import MapKit

protocol ChooseCarLocationViewControllerDelegate: class {
    func didUpdateLocation(coordinate: CLLocationCoordinate2D)
}

class ChooseCarLocationViewController: UIViewController, ReusableView {

    @IBOutlet weak var mapView: MKMapView!
    weak var delegate: ChooseCarLocationViewControllerDelegate?
    var coordinate: CLLocationCoordinate2D?
    var locationManager: CLLocationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureMap()
    }
    
    func configureMap() {
        locationManager.requestWhenInUseAuthorization()
        mapView.showsUserLocation = true
        
        if let coordinate = coordinate {
            let pinAnnotation = MKPointAnnotation()
            pinAnnotation.coordinate = coordinate
            mapView.addAnnotation(pinAnnotation)
            
            showRegionWithCoodinates(coordinate)
        } else {
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
            }
        }
    }

    @IBAction func buttonSaveLocationAction(_ sender: Any) {
        if let coordinate = coordinate {
            delegate?.didUpdateLocation(coordinate: coordinate)
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func gestureLongPressMapAction(_ gestureRecognizer: UILongPressGestureRecognizer) {
        if gestureRecognizer.state == .began {
            if !mapView.annotations.isEmpty {
                mapView.removeAnnotations(mapView.annotations)
            }
            
            let touchPoint = gestureRecognizer.location(in: mapView)
            let coordinate = mapView.convert(touchPoint, toCoordinateFrom: mapView)
            let annotation = MKPointAnnotation()
            annotation.coordinate = coordinate
            mapView.addAnnotation(annotation)
            self.coordinate = coordinate
        }
    }
    
    func showRegionWithCoodinates(_ mapCoordinates: CLLocationCoordinate2D) {
        var mapRegion = MKCoordinateRegion()
        mapRegion.center = mapCoordinates
        mapRegion.span.latitudeDelta = 0.2
        mapRegion.span.longitudeDelta = 0.2
        mapView.setRegion(mapRegion, animated: true)
    }
    
}

extension ChooseCarLocationViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let coordinate = locations.first?.coordinate {
            showRegionWithCoodinates(coordinate)
        }
        locationManager.stopUpdatingLocation()
    }
}
