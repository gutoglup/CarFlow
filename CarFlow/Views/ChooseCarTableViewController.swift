//
//  ChooseCarTableViewController.swift
//  CarFlow
//
//  Created by Augusto Reis on 01/07/2018.
//  Copyright © 2018 Augusto Reis. All rights reserved.
//

import UIKit

protocol ChooseCarTableViewControllerDelegate: class {
    func choosedCar(car: Car?)
}

class ChooseCarTableViewController: UITableViewController, ReusableView {
    
    var carList: [Car] = [Car]()
    weak var delegate: ChooseCarTableViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func buttonCancelAction(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table View Delegate -
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carList.count + 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(indexPath: indexPath) as NothingChooseCarTableViewCell
            return cell
        default:
            let cell = tableView.dequeueReusableCell(indexPath: indexPath) as ChooseCarTableViewCell
            
            let carItem = carList[indexPath.row - 1]
            cell.labelFabric.text = carItem.fabric
            cell.labelModel.text = carItem.model

            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            delegate?.choosedCar(car: nil)
        default:
            delegate?.choosedCar(car: carList[indexPath.row - 1])
        }
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0001
    }
}
