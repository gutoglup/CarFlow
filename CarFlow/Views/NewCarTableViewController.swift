//
//  NewCarTableViewController.swift
//  CarFlow
//
//  Created by Augusto Reis on 01/07/2018.
//  Copyright © 2018 Augusto Reis. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField
import CoreML
import Vision
import MapKit
import SwifterSwift
import NotificationBannerSwift

class NewCarTableViewController: UITableViewController {
    
    @IBOutlet weak var imageViewCar: UIImageView!
    @IBOutlet weak var textFieldModel: JVFloatLabeledTextField!
    @IBOutlet weak var textFieldFabric: JVFloatLabeledTextField!
    @IBOutlet weak var textFieldYear: JVFloatLabeledTextField!
    @IBOutlet weak var textFieldRegisterDate: JVFloatLabeledTextField!
    @IBOutlet weak var mapViewLocation: MKMapView!
    
    var mapCoordinates: CLLocationCoordinate2D?
    let locationManager: CLLocationManager = CLLocationManager()
    var yearsList: [Int] = [Int]()
    
    var yearDatePicker = UIPickerView(frame: .zero) 
    var datePicker = UIDatePicker(frame: .zero)
    
    // MARK: - CoreML Request -
    lazy var classificationRequest: VNCoreMLRequest = {
        do {
            let model = try VNCoreMLModel(for: CarRecognition().model)
            
            let request = VNCoreMLRequest(model: model, completionHandler: { [weak self] request, error in
                self?.resultClassification(for: request, error: error)
            })
            request.imageCropAndScaleOption = .centerCrop
            return request
        } catch {
            self.showBannerMessage(title: "Erro ao carregar o modelo do CoreML", type: .danger)
            fatalError()
        }
    }()
    
    // MARK: - Initialization Delegate -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureDatePicker()
        configureYearPicker()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        imageViewCar.layer.cornerRadius = self.imageViewCar.frame.size.height/2
        configureMap()
    }
    
    // MARK: - Year Picker -
    
    func configureYearPicker() {
        yearDatePicker.dataSource = self
        yearDatePicker.delegate = self
        
        let dateInitial = Date(timeIntervalSince1970: 0)
        let dateFinal = Date()
        
        for year in dateInitial.year...dateFinal.year { yearsList.append(year) }
        yearsList.reverse()
        
        textFieldYear.inputView = yearDatePicker
        configureToolbar(textFieldYear)
    }
    
    
    // MARK: - Date Picker -
    
    func configureDatePicker() {
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(changeValueDatePickerAction(datePicker:)), for: .valueChanged)
        textFieldRegisterDate.inputView = datePicker
        configureToolbar(textFieldRegisterDate)
    }
    
    func configureToolbar(_ textField: UITextField) {
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 42))
        toolbar.barStyle = .default
        
        let spaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let barButtonDone = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(buttonDoneAction(_:)))
        barButtonDone.tintColor = .black
        
        toolbar.setItems([spaceItem, barButtonDone], animated: true)
        textField.inputAccessoryView = toolbar
    }
    
    @objc func buttonDoneAction(_ barButtonItem: UIBarButtonItem) {
        textFieldYear.resignFirstResponder()
        textFieldRegisterDate.resignFirstResponder()
    }
    
    @objc func changeValueDatePickerAction(datePicker: UIDatePicker) {
        let date = datePicker.date.string(withFormat: "dd/MM/yyyy")
        textFieldRegisterDate.text = date
    }
    
    // MARK: - Choose image -
    
    @IBAction func buttonActionChooseImage(_ sender: Any) {
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            presentPhotoPicker(sourceType: .photoLibrary)
            return
        }
        
        let photoSourcePicker = UIAlertController()
        let takePhoto = UIAlertAction(title: "Tirar foto", style: .default) { [unowned self] _ in
            self.presentPhotoPicker(sourceType: .camera)
        }
        let choosePhoto = UIAlertAction(title: "Escolher da biblioteca", style: .default) { [unowned self] _ in
            self.presentPhotoPicker(sourceType: .photoLibrary)
        }
        
        photoSourcePicker.addAction(takePhoto)
        photoSourcePicker.addAction(choosePhoto)
        photoSourcePicker.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        
        present(photoSourcePicker, animated: true)
    }
    
    
    func presentPhotoPicker(sourceType: UIImagePickerControllerSourceType) {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = sourceType
        present(picker, animated: true)
    }
    
    // MARK: - Save Action -
    
    fileprivate func showBannerMessage(title: String, subtitle: String = "", type: BannerStyle) {
        DispatchQueue.main.async {
            let banner = NotificationBanner(title: title, style: type)
            banner.show()
        }
    }
    
    @IBAction func buttonSaveCarAction(_ sender: Any) {
        if  !textFieldModel.isEmpty,
            !textFieldFabric.isEmpty,
            !textFieldRegisterDate.isEmpty,
            !textFieldYear.isEmpty,
            let model = textFieldModel.text,
            let fabric = textFieldFabric.text,
            let year = textFieldYear.text?.int,
            let registerDate = textFieldRegisterDate.text?.date(withFormat: "dd/MM/yyyy"),
            let latitude = mapCoordinates?.latitude,
            let longitude = mapCoordinates?.longitude,
            let image = imageViewCar.image,
            let dataImage = UIImagePNGRepresentation(image) {
            
            let car = Car(model: model, fabric: fabric, year: year, latitude: latitude, longitude: longitude, date: registerDate, image: dataImage)
            car.saveCar { [weak self] (car, error) in
                
                if error != nil {
                    self?.showBannerMessage(title: "Erro ao salvar o registro", subtitle: "Favor verificar as informações", type: .danger)
                } else {
                    self?.showBannerMessage(title: "Registro salvo com sucesso", type: .success)
                    if let clearData = self?.clearData {
                        DispatchQueue.main.async(execute: clearData)
                    }
                }
            }
        } else {
            showBannerMessage(title: "Informações incompletas", subtitle: "Favor verificar as informações", type: .danger)
        }
    }
    
    func clearData() {
        textFieldModel.clear()
        textFieldFabric.clear()
        textFieldRegisterDate.clear()
        textFieldYear.clear()
        mapCoordinates = nil
        mapViewLocation.removeAnnotations(mapViewLocation.annotations)
        imageViewCar.image = #imageLiteral(resourceName: "carDefault")
    }
    
    // MARK: - MapKit -
    
    @IBAction func buttonChooseLocationAction(_ sender: Any) {
        self.performSegue(withIdentifier: ChooseCarLocationViewController.identifier, sender: nil)
    }
    
    func configureMap() {
        locationManager.requestWhenInUseAuthorization()
        mapViewLocation.showsUserLocation = true
        
        if let mapCoordinates = mapCoordinates {
            let pinAnnotation = MKPointAnnotation()
            pinAnnotation.coordinate = mapCoordinates
            mapViewLocation.addAnnotation(pinAnnotation)
            
            showRegionWithCoodinates(mapCoordinates)
        } else {
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
            }
        }
    }
    
    func showRegionWithCoodinates(_ mapCoordinates: CLLocationCoordinate2D) {
        var mapRegion = MKCoordinateRegion()
        mapRegion.center = mapCoordinates
        mapRegion.span.latitudeDelta = 0.2
        mapRegion.span.longitudeDelta = 0.2
        mapViewLocation.setRegion(mapRegion, animated: true)
    }
    
    // MARK: - CoreML -
    
    func carClassification(for image: UIImage) {
        
        guard let orientation = CGImagePropertyOrientation(rawValue: UInt32(image.imageOrientation.rawValue)),
              let ciImage = CIImage(image: image)
              else {
                self.showBannerMessage(title: "Erro ao recuperar a imagem", type: .danger)
                return
              }
        
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            let handler = VNImageRequestHandler(ciImage: ciImage, orientation: orientation)
            do {
                if let classificationRequest = self?.classificationRequest {
                    try handler.perform([classificationRequest])
                }
            } catch {
                self?.showBannerMessage(title: "Erro ao identificar o carro", type: .warning)
            }
        }
    }
    
    func resultClassification(for request: VNRequest, error: Error?) {
        DispatchQueue.main.async { [weak self] in
            guard let results = request.results else {
                return
            }
            let classifications = results as! [VNClassificationObservation]
            
            let carList = CarDataModel.carListRepresentation(observationList: classifications)
            self?.performSegue(withIdentifier: ChooseCarTableViewController.identifier, sender: carList)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  let navigationController = segue.destination as? UINavigationController,
            let chooseCarTableViewController = navigationController.topViewController as? ChooseCarTableViewController,
            let carList = sender as? [Car] {
            chooseCarTableViewController.carList = carList
            chooseCarTableViewController.delegate = self
        }
        
        if let chooseCarLocationViewController = segue.destination as? ChooseCarLocationViewController {
            chooseCarLocationViewController.delegate = self
            if let mapCoordinates = mapCoordinates {
                chooseCarLocationViewController.coordinate = mapCoordinates
            }
        }
    }
    
    // MARK: - TableView Delegate -
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0001
    }
}


extension NewCarTableViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    // MARK: - Handling Image Picker Selection
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        picker.dismiss(animated: true)
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        imageViewCar.image = image
        imageViewCar.layer.cornerRadius = self.imageViewCar.frame.size.height/2
        carClassification(for: image)
    }
}

extension NewCarTableViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let coordinate = locations.first?.coordinate {
            showRegionWithCoodinates(coordinate)
        }
        locationManager.stopUpdatingLocation()
    }
}

extension NewCarTableViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return yearsList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(yearsList[row])"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        textFieldYear.text = "\(yearsList[row])"
    }
    
}

extension NewCarTableViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textFieldYear {
            let row = yearDatePicker.selectedRow(inComponent: 0)
            textField.text = "\(yearsList[row])"
        }
        if textField == textFieldRegisterDate {
            let date = datePicker.date.string(withFormat: "dd/MM/yyyy")
            textField.text = date
        }
    }
}

extension NewCarTableViewController: ChooseCarTableViewControllerDelegate {
    func choosedCar(car: Car?) {
        if let car = car {
            textFieldModel.text = car.model
            textFieldFabric.text = car.fabric
        }
    }
}

extension NewCarTableViewController: ChooseCarLocationViewControllerDelegate {
    func didUpdateLocation(coordinate: CLLocationCoordinate2D) {
        mapCoordinates = coordinate
    }
}
