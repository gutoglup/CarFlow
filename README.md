---

# Projeto final iOS 3

## Descrição

O projeto consiste em cadastrar carros que foram vistos na rua. O aplicativo utiliza o framework `CoreML` e o `Vision` para tentar identificar o carro proposto. Assim salvando o Modelo, Marca, Ano, Foto e Data do registro no banco de dados local `RealmSwift`.

## Instruções

- Para executar o projeto é necessário ter o CocoaPods instalado no seu Mac.
- Execute o comando `pod install` para instalar as dependências do projeto.
- Após este passo, abra o arquivo `CarFlow.xcworkspace`.

## Modelos

Na pasta `CarFlow/CoreMLModel/` existe o arquivo `CarListDataModel.json` que são os modelos dos carros treinados pelo modelo de Machine Learning disponível para o reconhecimento dos carros.

## Arquitetura

Para este projeto foram utilizados os seguintes conceitos:

- MVC Apple
- Protocolos e Delegates
- MapKit
- CoreML e Vision
- UITableViewController, UIViewController, UITabBarController, UINavigationController
- Reconhecimento de gestos no mapa (UILongPressGestureRecognizer)
- Autolayout
- Banco de dados local (RealmSwift)
- Utilização de biblioteca de Fotos
